# 2.1 神经网络的参数

* 神经网络的**参数**: 是指神经元线上的权重`w`，用变量表示，一般会先随机生成这些参数。生成参数的方法是让`w`等于`tf.Variable`，把生成的方式写在括号里。

神经网络中常用的生成随机数/数组的函数有:


| 函数 | 说明 |
| --- | --- |
| `tf.random_normal()` | 生成正态分布随机数 |
| `tf.truncated_normal()` | 生成去掉过大偏离点的正态分布随机数 |
| `tf.random_uniform()` | 生成均匀分布随机数 |
| `tf.zeros` |  表示生成全 0 数组 |
| `tf.ones` | 表示生成全 1 数组 |
| `tf.fill` | 表示生成全定值数组 |
| `tf.constant` | 表示生成直接给定值的数组 |

举例

(1)生成正态分布随机数，形状两行三列，标准差是2，均值是0，随机种子是1。

```python
w=tf.Variable(tf.random_normal([2,3],stddev=2, mean=0, seed=1))
```

(2)去掉偏离过大的正态分布，也就是如果随机出来的数据偏离平均值超过两个 标准差，这个数据将重新生成。

```python
w=tf.Variable(tf.Truncated_normal([2,3],stddev=2, mean=0, seed=1))
```

(3)从一个均匀分布`[minval maxval)`中随机采样，注意定义域是左闭右开，即包含`minval`，不包含`maxval`。

```python
w=random_uniform(shape=7,minval=0,maxval=1,dtype=tf.int32，seed=1)
```

(4)除了生成随机数，还可以生成常量。

```python
# 生成[[0,0],[0,0],[0,0]]
tf.zeros([3,2],int32) 

#生成[[1,1],[1,1],[1,1]
tf.ones([3,2],int32)

# 生成[[6,6],[6,6],[6,6]]
tf.fill([3,2],6)

#生成[3,2,1]
tf.constant([3,2,1])
```

>注：
>1. 随机种子如果去掉每次生成的随机数将不一致。

>2. 如果没有特殊要求标准差、均值、随机种子是可以不写的。


